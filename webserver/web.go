package main

import (
	"encoding/json"
	"net/http"
	"rebate/cards"
	"rebate/rebatelib"
	"strconv"
	"strings"
)

// http://localhost:8080/search?keywords=telco,singtel&amount=14832
func main() {
	http.HandleFunc("/search", searchRebateRules)
	http.HandleFunc("/keywords/static", returnStatickKeywords)
	http.ListenAndServe(":8080", nil)
}

func searchRebateRules(writer http.ResponseWriter, request *http.Request) {
	amountStr := request.URL.Query().Get("amount")
	keywords := strings.Split(request.URL.Query().Get("keywords"), ",")
	if amountStr == "" {
		amountStr = "0"
	}

	amount, _ := strconv.ParseFloat(amountStr, 64)
	keywordSet := new(rebatelib.KeywordSet)
	keywordSet.ParseFromSlice(keywords)
	rebateRules := rebatelib.Search(cards.SupportedCards, keywordSet, amount)
	rebateRulesJSON, _ := json.Marshal(rebateRules)

	writer.Header().Set("Content-Type", "application/json")
	writer.Write(rebateRulesJSON)
	// fmt.Fprintf(writer, "%v\n", rebateRulesJson)
}

func returnStatickKeywords(writer http.ResponseWriter, request *http.Request) {

	keywords := make(map[string]interface{})
	keywordsJSON, _ := json.Marshal(rebateRules)
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(rebateRulesJSON)
}

type Option struct {
}

/*
{
	"results": [
	  {
		"id": 1,
		"text": "Option 1"
	  },
	  {
		"id": 2,
		"text": "Option 2",
		"selected": true
	  },
	  {
		"id": 3,
		"text": "Option 3",
		"disabled": true
	  }
	]
  }*/
