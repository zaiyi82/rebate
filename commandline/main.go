package main

import (
	"fmt"
	"os"
	"rebate/cards"
	"rebate/rebatelib"
	"strconv"
	"strings"
)

// example command line:
// go run main.go 200 "dining overseas" hospital
func main() {

	amount, _ := strconv.ParseFloat(os.Args[1], 64)
	keywordSet := new(rebatelib.KeywordSet)
	keywordsFromArgs := os.Args[2:]
	for i, keyword := range keywordsFromArgs {
		keywordsFromArgs[i] = strings.Trim(keyword, "\"")
	}
	keywordSet.ParseFromSlice(keywordsFromArgs)

	appliedRebateRules := rebatelib.Search(cards.SupportedCards, keywordSet, amount)
	if len(appliedRebateRules) == 0 {
		fmt.Println("No match found")
	}
	for i, appliedRebateRule := range appliedRebateRules {
		rebateRateDisplay := ""
		if appliedRebateRule.RebateUnit == "S$" {
			rebateRateDisplay = fmt.Sprintf("%.2f%%", appliedRebateRule.RebateRate*100)
		} else {
			rebateRateDisplay = fmt.Sprintf("%v %v per S$1", appliedRebateRule.RebateRate, appliedRebateRule.RebateUnit)
		}
		fmt.Printf("%v. %.2f%% S$%.2f %v (%v) - %v\n",
			i,
			appliedRebateRule.MaxActualRebateRate*100,
			appliedRebateRule.MaxRebateDollars,
			appliedRebateRule.CardName,
			rebateRateDisplay,
			appliedRebateRule.RuleName)
		if appliedRebateRule.StartDate != nil {
			fmt.Printf("\tStarts from: %v\n", appliedRebateRule.StartDate)
		}
		if appliedRebateRule.EndDate != nil {
			fmt.Printf("\tEnds by: %v\n", appliedRebateRule.EndDate)
		}
		if appliedRebateRule.RebateCap > 0 {
			fmt.Printf("\tRebate Cap: %v %v %v\n", appliedRebateRule.RebateCap, appliedRebateRule.RebateUnit, appliedRebateRule.RebateCapPeriod)
		}
		if appliedRebateRule.MinSpend > 0 {
			fmt.Printf("\tMin Spend: S$%v %v \n", appliedRebateRule.MinSpend, appliedRebateRule.MinSpendPeriod)
		}
		if len(appliedRebateRule.FinePrints) > 0 {
			fmt.Printf("\tFine prints: ")
			for i, finePrint := range appliedRebateRule.FinePrints {
				fmt.Printf("[%v] %v ", i, finePrint)
			}
			fmt.Printf("\n")
		}
		if len(appliedRebateRule.RebateDollars) > 1 {
			fmt.Printf("\tRebate Redemption:\n")
			for _, rebateDollars := range appliedRebateRule.RebateDollars {
				fmt.Printf("\t\tS$%.2f (%v)\n", rebateDollars.DollarAmount, rebateDollars.Description)
			}
		}
		fmt.Println()

	}
}
