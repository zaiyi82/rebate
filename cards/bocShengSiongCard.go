package cards

import (
	"rebate/rebatelib"
)

const bocShengSiongCardName = "BOC Sheng Siong Card"

var BocShengSiongCard = rebatelib.CardRules{
	CardName:      bocShengSiongCardName,
	EffectiveFrom: nil,
	LastUpdated:   NewTime(2017, 12, 31, 16, 55, 0),
	RebateRules: []rebatelib.RebateRule{
		rebatelib.RebateRule{
			CardName:        bocShengSiongCardName,
			RuleName:        "Sheng Siong Spend Base Rebate",
			Keywords:        []string{"grocery", "shengsiong", "sheng siong"},
			RebateRate:      0.07, // 7%
			RebateCap:       70,
			RebateCapPeriod: "per billing cycle",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "per billing cycle",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{},
		},
		rebatelib.RebateRule{
			CardName:        bocShengSiongCardName,
			RuleName:        "Sheng Siong Spend Additional Rebate Promo[0]",
			Keywords:        []string{"grocery", "shengsiong", "sheng siong"},
			RebateRate:      0.05, // 5% = 3% + 2%
			RebateCap:       20,
			RebateCapPeriod: "per billing cycle",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        400,
			MinSpendPeriod:  "per billing cycle",
			StartDate:       nil,
			EndDate:         NewTime(2018, 06, 30, 23, 59, 59),
			FinePrints:      []string{"charge at least 400 out-store spend (non Sheng Siong, retail spend)"},
		},
		rebatelib.RebateRule{
			CardName:        bocShengSiongCardName,
			RuleName:        "Sheng Siong Spend Additional Rebate[0]",
			Keywords:        []string{"grocery", "shengsiong", "sheng siong"},
			RebateRate:      0.03, // 3%
			RebateCap:       20,
			RebateCapPeriod: "per billing cycle",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        400,
			MinSpendPeriod:  "per billing cycle",
			StartDate:       NewTime(2018, 07, 01, 0, 0, 0),
			EndDate:         nil,
			FinePrints:      []string{"charge at least 400 out-store spend (non Sheng Siong, retail spend)"},
		},
		rebatelib.RebateRule{
			CardName:        bocShengSiongCardName,
			RuleName:        "Transport by cabs[0]",
			Keywords:        []string{"grab", "uber", "taxi"},
			RebateRate:      0.05, // 5%
			RebateCap:       20,
			RebateCapPeriod: "per billing cycle",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        400,
			MinSpendPeriod:  "per billing cycle",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{"grab, uber, taxi rides with MCC4121 - taxicabs and limousines"},
		},
		rebatelib.RebateRule{
			CardName:        bocShengSiongCardName,
			RuleName:        "Overseas spend",
			Keywords:        []string{"overseas"},
			RebateRate:      0.01, // 1%
			RebateCap:       0,
			RebateCapPeriod: "",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{"transactions made in foreign currencies"},
		},
		rebatelib.RebateRule{
			CardName:        bocShengSiongCardName,
			RuleName:        "Standard retail[0]",
			Keywords:        nil,
			RebateRate:      0.003, // 0.3%
			RebateCap:       0,
			RebateCapPeriod: "",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{"exclude: interest, annual fees, finance charges, cash advances, balance transfer, instalment plans, " +
				"tax payment, IRAS processing fee, GST, loading of prepaid accounts"},
		},
	},
}
