package cards

import (
	"rebate/rebatelib"
)

const maybankFamilyCardName = "Maybank Family Card"

var MaybankFamilyCard = rebatelib.CardRules{
	CardName:      maybankFamilyCardName,
	EffectiveFrom: NewTime(2017, 7, 1, 0, 0, 0),
	LastUpdated:   NewTime(2017, 12, 31, 17, 38, 0),
	RebateRules: []rebatelib.RebateRule{
		rebatelib.RebateRule{
			CardName: maybankFamilyCardName,
			RuleName: "Min 500/mht: Groceries[0], Bus/Train rides[1], Taxis, Petrol, Fast food delivery[2], Pharmacies[3], Selected Learning, retail, leisure[4]",
			Keywords: []string{"ntuc", "cold storage", "giant", "bus", "MRT", "taxi", "grab", "caltex", "esso", "shell", "spc",
				"foodpanda", "kfc", "mcdonlad's", "pizza hut", "unity", "guardian", "watsons", "popular", "toys 'r' us", "yamaha", "legoland", "theme parks"},
			RebateRate:      0.05,
			RebateCap:       600,
			RebateCapPeriod: "per calendar year",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        500,
			MinSpendPeriod:  "per calendar month",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{"Groceries: only applicable to MCC5411",
				"Bus/Train: register card with ABT", "Fast Food Delivery: KFC, McDonald's, Pizza Hut, Foodpanda",
				"Pharmacies: Unity NTUC Healthcare, Guardian, Watsons",
				"Merchants: Popular bookstore/online store, Toys 'R' Us, Yamaha music stores (not applicable to course fees), Legoland Malaysia, Theme parks in Malaysia"},
		},
		rebatelib.RebateRule{
			CardName: maybankFamilyCardName,
			RuleName: "Min 1000/mth: Groceries[0], Bus/Train rides[1], Taxis, Petrol, Fast food delivery[2], Pharmacies[3], Selected Learning, retail, leisure[4]",
			Keywords: []string{"ntuc", "cold storage", "giant", "bus", "MRT", "taxi", "grab", "caltex", "esso", "shell", "spc",
				"foodpanda", "kfc", "mcdonlad's", "pizza hut", "unity", "guardian", "watsons", "popular", "toys 'r' us", "yamaha", "legoland", "theme parks"},
			RebateRate:      0.08,
			RebateCap:       600,
			RebateCapPeriod: "per calendar year",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        1000,
			MinSpendPeriod:  "per calendar month",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{"Groceries: only applicable to MCC5411",
				"Bus/Train: register card with ABT", "Fast Food Delivery: KFC, McDonald's, Pizza Hut, Foodpanda",
				"Pharmacies: Unity NTUC Healthcare, Guardian, Watsons",
				"Merchants: Popular bookstore/online store, Toys 'R' Us, Yamaha music stores (not applicable to course fees), Legoland Malaysia, Theme parks in Malaysia"},
		},
		rebatelib.RebateRule{
			CardName:        maybankFamilyCardName,
			RuleName:        "Other Spend (including Instalment)",
			Keywords:        nil,
			RebateRate:      0.003, // 0.3%
			RebateCap:       600,
			RebateCapPeriod: "per calendar year",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{},
		},
	},
}
