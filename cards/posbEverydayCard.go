package cards

import (
	"rebate/rebatelib"
)

const posbEverydayCardName = "POSB Everyday"

var PosbEverydayCard = rebatelib.CardRules{
	CardName:      posbEverydayCardName,
	EffectiveFrom: NewTime(2017, 11, 9, 0, 0, 0),
	LastUpdated:   NewTime(2017, 12, 31, 18, 50, 0),

	RebateRules: []rebatelib.RebateRule{
		rebatelib.RebateRule{
			CardName:        posbEverydayCardName,
			RuleName:        "Groceries Sheng Siong",
			Keywords:        []string{"sheng siong", "grocery"},
			RebateRate:      0.05, // 5%
			RebateCap:       50,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "Daily$",
			RebateToDollar:  ConvertDailyDollarToDollar,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         NewTime(2018, 3, 31, 23, 59, 59),
			FinePrints:      []string{},
		},
		rebatelib.RebateRule{
			CardName:        posbEverydayCardName,
			RuleName:        "SP Utillities[0], Starhub bills",
			Keywords:        []string{"utility", "starhub"},
			RebateRate:      0.01, // 1%
			RebateCap:       1,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "Daily$",
			RebateToDollar:  ConvertDailyDollarToDollar,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         NewTime(2018, 3, 31, 23, 59, 59),
			FinePrints: []string{"For SP Group, recurring charges are applicable only to accounts " +
				"under the name of the main or supplementary cardholder"},
		},
		rebatelib.RebateRule{
			CardName:        posbEverydayCardName,
			RuleName:        "Watsons",
			Keywords:        []string{"watsons"},
			RebateRate:      0.03, // 3%
			RebateCap:       0,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "Daily$",
			RebateToDollar:  ConvertDailyDollarToDollar,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         NewTime(2018, 3, 31, 23, 59, 59),
			FinePrints:      []string{},
		},
		rebatelib.RebateRule{
			CardName:        posbEverydayCardName,
			RuleName:        "Overseas Spend Promo",
			Keywords:        []string{"overseas"},
			RebateRate:      0.05, // 5%
			RebateCap:       50,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "Daily$",
			RebateToDollar:  ConvertDailyDollarToDollar,
			MinSpend:        1000,
			MinSpendPeriod:  "per calendar month",
			StartDate:       NewTime(2017, 11, 1, 0, 0, 0),
			EndDate:         NewTime(2018, 2, 28, 23, 59, 59),
			FinePrints:      []string{},
		},
		rebatelib.RebateRule{
			CardName:        posbEverydayCardName,
			RuleName:        "Meidcal Promo [0][1]",
			Keywords:        []string{"medical", "hospital", "dental"},
			RebateRate:      0.03, // 3%
			RebateCap:       50,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "Daily$",
			RebateToDollar:  ConvertDailyDollarToDollar,
			MinSpend:        500,
			MinSpendPeriod:  "per calendar month",
			StartDate:       NewTime(2017, 7, 13, 0, 0, 0),
			EndDate:         NewTime(2018, 7, 31, 23, 59, 59),
			FinePrints: []string{"applicable to POSB Smiley Child Development Account Trustees",
				"Medical spend: includes local/ on-site spend on medical services made at all hospitals, medical" +
					"and dental clinics in Singapore, but excludes Card-not-present transactions such as online transactions, " +
					"mail/phone order and all spends at healthcare related merchants such as, but limited to, Watsons, " +
					"Guardian, GNC, Hockhua Tonic, NTUC Unity and Eu Yan Sang etc"},
		},
		rebatelib.RebateRule{
			CardName:        posbEverydayCardName,
			RuleName:        "Helping Promo [0][1]",
			Keywords:        []string{"helping", "cleaning"},
			RebateRate:      0.5, // 50% off
			RebateCap:       0,
			RebateCapPeriod: "first recurring cleaning",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       NewTime(2017, 7, 1, 0, 0, 0),
			EndDate:         NewTime(2018, 6, 30, 23, 59, 59),
			FinePrints: []string{"Only for new customers with weekly or fortnightly bookings with a minimum of 3 hours booking; " +
				"Full payment must be made with POSB Everyday card"},
		},
		rebatelib.RebateRule{
			CardName:        posbEverydayCardName,
			RuleName:        "Standard[0]",
			Keywords:        nil,
			RebateRate:      0.003, // 0.3%
			RebateCap:       0,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "Daily$",
			RebateToDollar:  ConvertDailyDollarToDollar,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         NewTime(2018, 3, 31, 23, 59, 59),
			FinePrints: []string{"Disqualified for earning rebates & not eligible spend for card promotions: " +
				"payments made via telephone or mail order; " +
				"payments to government institution; " +
				"payments to financial institutions (including banks and brokerages); " +
				"(payments to insurance companies; " +
				"utility bill payments; " +
				"donations; " +
				"payment of funds to prepaid accounts including but not limited to EZ link, Telco topup and merchants who are categorized as “payment service providers " +
				"payments to schools, hospitals, professional service providers and payment for parking lots; " +
				"payments made via AXS and SAM machines, online/iBanking bill payment transactions, EZ-Reload (Auto Top-Up) transactions and eNets transactions; " +
				"NETS purchases; " +
				"funds transfer; " +
				"cash withdrawals; " +
				"fees charged by DBS; and " +
				"any other transaction determined by DBS from time to time. "},
		},
	},
}

// ConvertDailyDollarToDollar converts POSB Daily$ to dollar amount
func ConvertDailyDollarToDollar(rebateAmount float64) []rebatelib.RebateRedemption {
	return []rebatelib.RebateRedemption{
		rebatelib.RebateRedemption{rebateAmount, "Redeem at Watsons or Sheng Siong"},
	}
}
