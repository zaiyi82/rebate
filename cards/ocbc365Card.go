package cards

import (
	"rebate/rebatelib"
)

const (
	ocbc365CardName       = "OCBC 365"
	min600SpendExclusions = "S$600 monthly spend excludes annual card fees, cash-on-instalments," +
		"instalment payment plan, PayLite, interest, late payment charges, tax payments, cash advances, " +
		"balance transfers, other fees & charges, bill payments made via internet banking or AXS network"
	diningDef = "Dining category: MCC 5812 (Restaraunts and eating places, some pubs), " +
		"MCC 5814 (Fast food restaruants), MCC 5811 (Caterers). Excludes: MCC 5813 (Drinking Places)"
)

var Ocbc365Card = rebatelib.CardRules{
	CardName:      ocbc365CardName,
	EffectiveFrom: NewTime(2017, 11, 9, 0, 0, 0),
	LastUpdated:   NewTime(2017, 12, 3, 18, 26, 0),
	RebateRules: []rebatelib.RebateRule{
		rebatelib.RebateRule{
			CardName:        ocbc365CardName,
			RuleName:        "Weekday Dining[1] Locally, Dining[1] Overseas",
			Keywords:        []string{"dining", "dining overseas"},
			RebateRate:      0.03,
			RebateCap:       80,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        600,
			MinSpendPeriod:  "per calendar month, with exclusions[0]",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{min600SpendExclusions, diningDef}},
		rebatelib.RebateRule{
			CardName:        ocbc365CardName,
			RuleName:        "Weekend Dining[1] Locally",
			Keywords:        []string{"dining"},
			RebateRate:      0.06,
			RebateCap:       80,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        600,
			MinSpendPeriod:  "per calendar month, with exclusions[0]",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{min600SpendExclusions, diningDef}},
		rebatelib.RebateRule{
			CardName:        ocbc365CardName,
			RuleName:        "Petrol @ Caltex",
			Keywords:        []string{"petrol", "caltex"},
			RebateRate:      0.23,
			RebateCap:       80,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        600,
			MinSpendPeriod:  "per calendar month, with exclusions[0]",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{
				min600SpendExclusions,
				/*1*/ "16% upfront Platinum 98 discount " +
					"5% cashback on nett fuel amount " +
					"$10 rebate with min $300 nett fuel amount (= 2.8% on gross fuel spend)"}},
		rebatelib.RebateRule{
			CardName:        ocbc365CardName,
			RuleName:        "Petrol (non Caltex)",
			Keywords:        []string{"petrol"},
			RebateRate:      0.05,
			RebateCap:       80,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        600,
			MinSpendPeriod:  "per calendar month, with exclusions[0]",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{min600SpendExclusions}},
		rebatelib.RebateRule{
			CardName:        ocbc365CardName,
			RuleName:        "Online Shopping[1], Grocery[2], Recurring Telco bill",
			Keywords:        []string{"online", "grocery", "telco", "singtel", "starhub", "m1"},
			RebateRate:      0.03,
			RebateCap:       80,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        600,
			MinSpendPeriod:  "per calendar month, with exclusions[0]",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{
				min600SpendExclusions,
				/*1*/ "Internet transactions via Visa/Master network. Excludes: purchases via telphone or mail order; subscription and recurring payments; " +
					"payments to gov institutions, financial institutions (including banks/brokerages), insurance companies; " +
					"utility bill payments; donations; funds to prepad accounts 'payment service providers'; " +
					"to schools, hospitals, professional service providers, parking lots, membership fees to clubs; " +
					"payments via online banking",
				/*2*/ "Groceries, supermarkets, hypermarkets with MCC 5411"}},
		rebatelib.RebateRule{
			CardName:        ocbc365CardName,
			RuleName:        "Medical (OCBC Bank Child Dev Account Trustees)",
			Keywords:        []string{"medical", "hospital"},
			RebateRate:      0.03,
			RebateCap:       80,
			RebateCapPeriod: "per calendar month",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        600,
			MinSpendPeriod:  "per calendar month, with exclusions[0]",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{
				min600SpendExclusions,
				"Applicable only for principal cardmembers who are also OCBC Bank Child Dev Account Trustees"}},
		rebatelib.RebateRule{
			CardName:        ocbc365CardName,
			RuleName:        "Standard [0]",
			Keywords:        nil,
			RebateRate:      0.003,
			RebateCap:       0,
			RebateCapPeriod: "",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{
				"Except transactions on charity/religious orgs, cleaning, maintenance, " +
					"janitorial service, real estate agents"}},
	},
}
