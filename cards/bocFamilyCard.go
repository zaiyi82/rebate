package cards

import (
	"rebate/rebatelib"
)

const (
	bocFamilyCardName     = "BOC Family Card (Main/Sub)"
	bocFamilyCardNameMain = "BOC Family Card (Main)"
	bocFamilyCardNameSub  = "BOC Family Card (Sub)"
)

var BocFamilyCard = rebatelib.CardRules{
	CardName:      bocFamilyCardName,
	EffectiveFrom: nil,
	LastUpdated:   NewTime(2017, 12, 15, 16, 51, 0),
	RebateRules: []rebatelib.RebateRule{
		// BOC Family Card (Main)
		rebatelib.RebateRule{
			CardName:        bocFamilyCardName,
			RuleName:        "Everyday Dining including Overseas (MCC 5812, 5814)",
			Keywords:        []string{"dining", "restaurant", "fast food", "cafe", "overseas dining"},
			RebateRate:      0.07, // 7%
			RebateCap:       100,
			RebateCapPeriod: "per billing cycle",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        500,
			MinSpendPeriod:  "per billing cycle",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{"Exclude F&B outlets in hotels"}},
		rebatelib.RebateRule{
			CardName:        bocFamilyCardName,
			RuleName:        "Hospital Bills (MCC 8062)[0], Supermarkets (MCC 5411), Selected Merchants[1]",
			Keywords:        []string{"hospital", "supermarket", "Best Denki", "Big Box", "Unity Pharmacy", "Watsons", "Toys 'R' Us", "Popular"},
			RebateRate:      0.05, // 5%
			RebateCap:       100,
			RebateCapPeriod: "per billing cycle",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        500,
			MinSpendPeriod:  "per billing cycle",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{"on-site transactions made at hospitals in Singapore with MCC 8062 - hospitals",
				"Selected Merchants: Best Denki; Popular bookstore; Watsons; Unity pharmacy; Toys 'R' Us; " +
					"Big Box (jurong east shopping mall) with MCC541 (grocery stores and supermarkets) MCC5712 (furniture, home furnishins, equipments) MCC5732 (electronics) MCC5912 (drug stores and pharmacies). " +
					"Exclude retail stores in schools, roadshows, expo, events"}},
		rebatelib.RebateRule{
			CardName:        bocFamilyCardName,
			RuleName:        "Online Purchases[0]",
			Keywords:        []string{"online"},
			RebateRate:      0.05, // 5%
			RebateCap:       30,
			RebateCapPeriod: "per billing cycle",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        500,
			MinSpendPeriod:  "per billing cycle",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{"Exclude payments to gov, institutions, bills, donations, parking, ezlink, axs/sam, helper service"}},
		rebatelib.RebateRule{
			CardName:        bocFamilyCardName,
			RuleName:        "NETS FlashPay Auto Top Up[0]",
			Keywords:        []string{"NETS flashpay auto-topup"},
			RebateRate:      0.05, // 5%
			RebateCap:       100,
			RebateCapPeriod: "per billing cycle",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        500,
			MinSpendPeriod:  "per billing cycle",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{"Limited to first 4 top-ups"}},
		rebatelib.RebateRule{
			CardName:        bocFamilyCardName,
			RuleName:        "Telco Bills",
			Keywords:        []string{"telco", "singtel", "starhub", "m1"},
			RebateRate:      0.01, // 1%
			RebateCap:       100,
			RebateCapPeriod: "per billing cycle",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        500,
			MinSpendPeriod:  "per billing cycle",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{}},
		rebatelib.RebateRule{
			CardName:        bocFamilyCardNameMain,
			RuleName:        "Standard retail[0]",
			Keywords:        nil,
			RebateRate:      0.005, // 0.5%
			RebateCap:       100,
			RebateCapPeriod: "",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{"retail expenses"}},

		// BOC Family Card (Sub)
		rebatelib.RebateRule{
			CardName:        bocFamilyCardNameSub,
			RuleName:        "Additional Subcard Rebate",
			Keywords:        nil,
			RebateRate:      0.02, // 2%
			RebateCap:       20,
			RebateCapPeriod: "per billing cycle",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        200,
			MinSpendPeriod:  "per billing cycle",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{"Exclude spend on categories that earn higher rates",
				"Combined main & sub cards meet $500/mth + sub card meets $200/mth", "Awarded quarterly"}},
		rebatelib.RebateRule{
			CardName:        bocFamilyCardNameSub,
			RuleName:        "Standard retail[0]",
			Keywords:        nil,
			RebateRate:      0.005,
			RebateCap:       0,
			RebateCapPeriod: "",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints:      []string{"retail expenses"}},
	},
}
