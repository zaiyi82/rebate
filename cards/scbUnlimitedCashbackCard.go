package cards

import (
	"rebate/rebatelib"
)

const scbUnlimitedCashBackCardName = "Standard Chartered Unlimited Cashback"

var ScbUnlimitedCashBackCard = rebatelib.CardRules{
	CardName:      scbUnlimitedCashBackCardName,
	EffectiveFrom: NewTime(2017, 11, 9, 0, 0, 0),
	LastUpdated:   NewTime(2017, 12, 31, 22, 13, 0),
	RebateRules: []rebatelib.RebateRule{
		rebatelib.RebateRule{
			CardName:        scbUnlimitedCashBackCardName,
			RuleName:        "Standard Retail[0]",
			Keywords:        nil,
			RebateRate:      0.015,
			RebateCap:       0,
			RebateCapPeriod: "",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{"transactions are not Eligible Retail Transactions and will not earn Unlimited Cashback: " +
				"a) any cash advance; " +
				"b) any fees and charges (including annual fees, late payment fees, interest charges, and or finance charges) charged to your Card; " +
				"c) any balance or fund transfer; " +
				"d) any monthly instalment of an EasyPay transaction; " +
				"e) any amount charged to your Card (and/or your supplementary credit card) that is subsequently cancelled, voided, refunded or reversed; " +
				"f) recurring payments or payments made to all billing organisations using Standard Chartered Online Banking; " +
				"g) AXS or ATM transactions made using the Card; " +
				"h) amounts which have been rolled over from the preceding months’ statements; " +
				"i) tax refunds credited into your Card account (and/or your supplementary credit card account); " +
				"j) any tax payments charged to your Card (and/or your supplementary credit card); " +
				"k) any insurance premiums charged to your Card (and/or your supplementary credit card); and " +
				"l) any top-ups or payment of funds to any prepaid cards (with the exception of EZ-Reload charged to your Card) and any prepaid accounts including without limitation to the following accounts or any other accounts as we may specify from time to time: EZ LINK PTE LTD.. etc"},
		},
	},
}
