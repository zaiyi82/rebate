package cards

import (
	"rebate/rebatelib"
)

const citiCashbackCardName = "Citi Cashback"

var CitiCashbackCard = rebatelib.CardRules{
	CardName:      citiCashbackCardName,
	EffectiveFrom: NewTime(2017, 11, 9, 0, 0, 0),
	LastUpdated:   NewTime(2017, 12, 31, 17, 24, 0),

	RebateRules: []rebatelib.RebateRule{
		rebatelib.RebateRule{
			CardName:        citiCashbackCardName,
			RuleName:        "Dining[1], Groceries[2] and Petrol[3] daily, worldwide[0]",
			Keywords:        []string{"dining", "dining overseas", "deliveroo", "grocery", "grocery overseas", "redmart", "petrol"},
			RebateRate:      0.08, // 8% = 0.25% + 7.75%
			RebateCap:       25,   // 25/0.0775 = 322 max expense per category
			RebateCapPeriod: "per cateogry statement month",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        888,
			MinSpendPeriod:  "per statement month",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{"0.25% on retail purchases, and 7.75% additional cashback. $25 cap with 7.75% = $322 max spend per category per month",
				"Dining: MCC 5811 (Caterers), MCC 5812 (Eating Places, Restaurants), MCC 5813 (Drinking Places), MCC 5814 (Fast Food Restaurants)",
				"Grocery: MCC 5411 (Grocery Stores, Supermarkets), MCC 5499 (Miscellaneous Food Stores - Convenience Stores and Specialty Markets)",
				"Petrol: MCC 5541 (Service Stations), MCC 5542 (Automated Fuel Dispensers)"}},
		rebatelib.RebateRule{
			CardName:        citiCashbackCardName,
			RuleName:        "Grab Promition Jul 2017 - Apr 2018",
			Keywords:        []string{"grab"},
			RebateRate:      0.08,
			RebateCap:       25,
			RebateCapPeriod: "per cateogry statement month",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        888,
			MinSpendPeriod:  "per statement month",
			StartDate:       NewTime(2017, 7, 7, 0, 0, 0),
			EndDate:         NewTime(2018, 4, 6, 23, 59, 59),
			FinePrints:      []string{"0.25% on retail purchases, and 7.75% additional cashback"}},
		rebatelib.RebateRule{
			CardName:        citiCashbackCardName,
			RuleName:        "Foodpanda Promotion existing customers[0]",
			Keywords:        []string{"foodpanda"},
			RebateRate:      0.10,
			RebateCap:       30,
			RebateCapPeriod: "per redemption",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        35,
			MinSpendPeriod:  "per order",
			StartDate:       nil,
			EndDate:         NewTime(2017, 12, 31, 23, 59, 59),
			FinePrints:      []string{"use C10XXXXXX (first 6 digits of citi card)"}},
		rebatelib.RebateRule{
			CardName:        citiCashbackCardName,
			RuleName:        "Expedia Promition",
			Keywords:        []string{"expedia"},
			RebateRate:      0.025, // 2.5%
			RebateCap:       25,
			RebateCapPeriod: "per cateogry statement month",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         NewTime(2018, 2, 28, 23, 59, 59),
			FinePrints:      []string{"www.expedia.com.sg/bonusrewards, book participating flights or hotels"}},
		rebatelib.RebateRule{
			CardName:        citiCashbackCardName,
			RuleName:        "Retail[0]",
			Keywords:        nil,
			RebateRate:      0.0025,
			RebateCap:       0,
			RebateCapPeriod: "",
			RebateUnit:      "S$",
			RebateToDollar:  ConvertPercentageBasedRebateToDollor,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{"Excludes: Payments of annual card membership fees, interest, late payment charges, GST, cash advances, quick" +
				"cash, balance transfers, equal payment plan transactions, income tax payments, bill payments/funds" +
				"transfer using the card as source of funds, and other late payment fees, interest and other service/miscellaneous fees or charges"}},
	},
}
