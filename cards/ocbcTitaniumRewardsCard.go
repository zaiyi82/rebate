package cards

import (
	"rebate/rebatelib"
)

const ocbcTitaniumRewardsCardName = "OCBC Titanium Rewards Card"

var OcbcTitaniumRewardsCard = rebatelib.CardRules{
	CardName:      ocbcTitaniumRewardsCardName,
	EffectiveFrom: nil,
	LastUpdated:   NewTime(2017, 11, 9, 0, 0, 0),
	RebateRules: []rebatelib.RebateRule{
		rebatelib.RebateRule{
			CardName:        ocbcTitaniumRewardsCardName,
			RuleName:        "Spend locally/overseas, online/offline[0][1]",
			Keywords:        []string{"clothes", "bag", "shoes", "gardget", "department store", "electronics", "baby wear", "children wear"},
			RebateRate:      10,
			RebateCap:       120000,
			RebateCapPeriod: "per card anniversary",
			RebateUnit:      "OCBC$",
			RebateToDollar:  ConvertOCBCDollarToDollar,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{"MCC 5611: Men’s and Boys’ Clothing and Accessories Stores. " +
				"MCC 5621: Women’s Ready to Wear Stores. " +
				"MCC 5631: Women’s Accessory and Speciality Stores. " +
				"MCC 5641: Children’s and Infants’ Wear Stores. " +
				"MCC 5651: Family Clothing Stores. " +
				"MCC 5661: Shoe Stores. " +
				"MCC 5691: Men’s and Women’s Clothing Stores. " +
				"MCC 5045: Computers, Peripherals, and Software. " +
				"MCC 5732: Electronics Stores. " +
				"MCC 5699: Miscellaneous Apparel and Accessory Shops." +
				"MCC 5311: Department Stores. ",
				"1 OCBC$ credited in current month and 9 OCBC$ credited in next calendar month"}},
		rebatelib.RebateRule{
			CardName:        ocbcTitaniumRewardsCardName,
			RuleName:        "Personal care[0][2], mobile payments[1][2]",
			Keywords:        []string{"pharmacy", "apple pay", ""},
			RebateRate:      10,
			RebateCap:       120000,
			RebateCapPeriod: "per year",
			RebateUnit:      "OCBC$",
			RebateToDollar:  ConvertOCBCDollarToDollar,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       NewTime(2016, 10, 17, 0, 0, 0),
			EndDate:         NewTime(2018, 12, 31, 23, 59, 59),
			FinePrints: []string{
				"MCC 5912: Drug Stores and Pharmacies",
				"Android Pay, Apple Pay, Samsung Pay. Excluding: online & in-app purchases, annual fees, instalment plans, " +
					"tax payment, paylite payments, cash advances",
				"1 OCBC$ credited in current month and 9 OCBC$ credited in next calendar month"}},
		rebatelib.RebateRule{
			CardName:        ocbcTitaniumRewardsCardName,
			RuleName:        "Retail Spend[0]",
			Keywords:        nil,
			RebateRate:      1,
			RebateCap:       120000,
			RebateCapPeriod: "per year",
			RebateUnit:      "OCBC$",
			RebateToDollar:  ConvertOCBCDollarToDollar,
			MinSpend:        0,
			MinSpendPeriod:  "",
			StartDate:       nil,
			EndDate:         nil,
			FinePrints: []string{"Exclude: Annual Card fees, Cash-on-Instalment, Instalment Payment Plan, PayLite, extended payment " +
				"plan, interest, late payment charges, tax payments, Cash Advances, Balance Transfers, bill " +
				"payments made via Internet Banking, bill payments made via AXS and other fees and charges"}},
	},
}

// ConvertOCBCDollarToDollar converts OCBC$ to dollar amount
func ConvertOCBCDollarToDollar(rebateAmount float64) []rebatelib.RebateRedemption {
	return []rebatelib.RebateRedemption{
		rebatelib.RebateRedemption{rebateAmount / 350, "7000 OCBC$ -> S$20 NTUC Fiarprice Voucher"},
		rebatelib.RebateRedemption{rebateAmount / 350, "3500 OCBC$ -> S$10 Robisons group voucher"},
		rebatelib.RebateRedemption{rebateAmount / 350, "1750 OCBC$ -> S$5 off from Zara purchase"},
		rebatelib.RebateRedemption{rebateAmount / 350, "3500 OCBC$ -> S$10 off Swensens bill"},
		rebatelib.RebateRedemption{rebateAmount / 360, "3600 OCBC$ -> $OCBC Titanium cash"},
		rebatelib.RebateRedemption{rebateAmount / 370, "3700 OCBC$ -> S$10 off Tokyu hand bill"},
	}
}
