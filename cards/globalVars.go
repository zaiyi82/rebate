package cards

import (
	"rebate/rebatelib"
	"time"
)

// list supported cards
var SupportedCards = []rebatelib.CardRules{
	ScbUnlimitedCashBackCard,
	BocFamilyCard,
	BocShengSiongCard,
	Ocbc365Card,
	OcbcTitaniumRewardsCard,
	CitiCashbackCard,
	MaybankFamilyCard,
	PosbEverydayCard}

// ConvertPercentageBasedRebateToDollor is the default converter for dollar based rebate into actual dollar (which is exactly the same value)
func ConvertPercentageBasedRebateToDollor(rebateAmount float64) []rebatelib.RebateRedemption {
	return []rebatelib.RebateRedemption{
		rebatelib.RebateRedemption{rebateAmount, ""},
	}
}

// constant timezone, for last updated fields

func NewTime(year int, month time.Month, day int, hour int, minute int, second int) *time.Time {
	sgt, _ := time.LoadLocation("Asia/Singapore")
	time := time.Date(year, month, day, hour, minute, second, 0, sgt)
	return &time
}
