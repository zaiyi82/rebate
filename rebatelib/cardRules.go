package rebatelib

import "time"

// CardRules is the collection of rebate rules of a single card
type CardRules struct {
	CardName      string
	EffectiveFrom *time.Time
	LastUpdated   *time.Time
	RebateRules   []RebateRule
}
