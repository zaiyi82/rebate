package rebatelib

import (
	"testing"
)

func TestKeywordSetContains(t *testing.T) {
	keywordSet := new(KeywordSet)
	keywordSet.keywords = make(map[string]struct{}, 10)
	keywordSet.keywords["hospital"] = struct{}{}
	keywordSet.keywords["dining"] = struct{}{}
	keywordSet.keywords["medical"] = struct{}{}

	if keywordSet.Contains("Hospital") == false {
		t.Log("keywordSet should contain 'Hospital'")
		t.Fail()
	}
	if keywordSet.Contains("shopping") == true {
		t.Log("keywordSet should not contain 'shopping'")
		t.Fail()
	}
}
func TestKeywordSetParseFromSlice(t *testing.T) {
	keywords := []string{"hospital", "dining"}
	keywordSet := new(KeywordSet)
	keywordSet.ParseFromSlice(keywords)

	if len(keywordSet.keywords) != 2 {
		t.Log("keywordSet should contain 2 items")
		t.Fail()
	}
	if keywordSet.Contains("hospital") == false {
		t.Log("keywordSet should contain `hospital`")
		t.Fail()
	}
	if keywordSet.Contains("dining") == false {
		t.Log("keywordSet should contain `dining`")
		t.Fail()
	}
	if keywordSet.Contains("telco") == true {
		t.Log("keywordSet should not contain `telco`")
		t.Fail()
	}
}
