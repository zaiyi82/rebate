package rebatelib

import (
	"testing"
	"time"
)

func TestRebateRuleMatches(t *testing.T) {

	rebateRule1 := RebateRule{
		CardName:       "SCB",
		RuleName:       "Standard",
		Keywords:       []string{"hospital", "grocery"},
		RebateRate:     0.015,
		RebateCap:      0,
		MinSpend:       0,
		MinSpendPeriod: "",
		StartDate:      nil,
		EndDate:        nil,
		FinePrints:     []string{}}

	keywordSet := new(KeywordSet)

	// 1. rule match because of matched keywords, case insensitive

	keywordSet.ParseFromSlice([]string{"Hospital", "medical"})
	if rebateRule1.Matches(keywordSet) == false {
		t.Log("rebateRule1 should match [hospital, meidcal]")
		t.Fail()
	}

	// 2. rule not matched because lack of matched keywords

	keywordSet.ParseFromSlice([]string{"Telco", "singtel"})
	if rebateRule1.Matches(keywordSet) == true {
		t.Log("rebateRule1 should not match [telco, singtel]")
		t.Fail()
	}

	// 3. rule not matched due to out of applicable date range

	// already passed
	pastDate := time.Now().AddDate(0, 0, -1)
	rebateRule2 := RebateRule{
		CardName:       "SCB",
		RuleName:       "Standard",
		Keywords:       []string{"hospital", "grocery"},
		RebateRate:     0.015,
		RebateCap:      0,
		MinSpend:       0,
		MinSpendPeriod: "",
		StartDate:      nil,
		EndDate:        &pastDate,
		FinePrints:     []string{}}

	keywordSet.ParseFromSlice([]string{"hospital"})
	if rebateRule2.Matches(keywordSet) == true {
		t.Log("rebateRule2 should not match because of end date is in the past")
		t.Fail()
	}

	// 4. rule matched: within date range

	startDate := time.Now().AddDate(0, 0, -1)
	endDate := time.Now().AddDate(0, 0, +1)
	rebateRule3 := RebateRule{
		CardName:       "SCB",
		RuleName:       "Standard",
		Keywords:       []string{"hospital", "grocery"},
		RebateRate:     0.015,
		RebateCap:      0,
		MinSpend:       0,
		MinSpendPeriod: "",
		StartDate:      &startDate,
		EndDate:        &endDate,
		FinePrints:     []string{}}

	keywordSet.ParseFromSlice([]string{"hospital"})
	if rebateRule3.Matches(keywordSet) == false {
		t.Log("rebateRule3 should match because within date range")
		t.Fail()
	}
}

func TestRebateRuleApplyAmount(t *testing.T) {

	rebateRule := RebateRule{
		CardName:       "SCB",
		RuleName:       "Standard",
		Keywords:       []string{"hospital", "grocery"},
		RebateRate:     0.05,
		RebateCap:      100,
		MinSpend:       500,
		MinSpendPeriod: "per calendar month",
		StartDate:      nil,
		EndDate:        nil,
		FinePrints:     []string{}}

	// 1. amount with rebate within cap

	appliedRebateRule := rebateRule.ApplyAmount(1900)
	if appliedRebateRule.ActualRebate != 95 ||
		appliedRebateRule.ActualRebateRate != 0.05 {
		t.Log("actual rebate should be 95 and actual rebate rate should be 0.05")
	}

	// 2. amount with rebate exceeding cap

	appliedRebateRule = rebateRule.ApplyAmount(2500)
	if appliedRebateRule.ActualRebate != 100 ||
		appliedRebateRule.ActualRebateRate != 0.04 {
		t.Log("actual rebate should be 100 and actual rebate rate should be 0.04")
	}

}
