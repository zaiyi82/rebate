package rebatelib

import (
	"strings"
)

// KeywordSet is a set of keywords
type KeywordSet struct {
	keywords map[string]struct{}
}

// ParseFromSlice translates a keywords slice into KeywordSet. Keywords are lower-cased in this process.
func (keywordSet *KeywordSet) ParseFromSlice(keywords []string) {
	set := make(map[string]struct{}, len(keywords))
	for _, s := range keywords {
		set[strings.ToLower(s)] = struct{}{}
	}
	keywordSet.keywords = set
}

// Contains checks whether a word exists in the keyword set. always compared based on lowercase value
func (keywordSet *KeywordSet) Contains(word string) bool {
	_, ok := keywordSet.keywords[strings.ToLower(word)]
	return ok
}
