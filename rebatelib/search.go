package rebatelib

import (
	"sort"
)

// Search searches for rebate rules that are applicable, given the set of keywords and dollar amount
func Search(supportedCards []CardRules, keywordSet *KeywordSet, spendDollars float64) []AppliedRebateRule {

	appliedRules := make([]AppliedRebateRule, 0)

	// search for matched rules
	for _, card := range supportedCards {
		for _, rebateRule := range card.RebateRules {
			if rebateRule.Matches(keywordSet) {
				appliedRules = append(appliedRules, rebateRule.ApplySpend(spendDollars))
			}
		}
	}

	// sort by rebate rates from high to low
	sort.Slice(appliedRules, func(i, j int) bool {
		return appliedRules[i].MaxActualRebateRate > appliedRules[j].MaxActualRebateRate
	})

	return appliedRules
}
