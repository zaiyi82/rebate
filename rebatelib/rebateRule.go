package rebatelib

import (
	"math"
	"time"
)

// RebateRule represents one rebate rate and its context (e.g., card issuer) and conditions (e.g., keywords to match).
type RebateRule struct {
	CardName        string
	RuleName        string
	Keywords        []string                         // for matching keyword from query. case insensitive
	RebateRate      float64                          // the ratio between rebate amount (not necessarily dollars) and spent amount.
	RebateCap       float64                          // dollar amount for max rebate per cycle. 0 means no cap. unit RebateUnit
	RebateCapPeriod string                           // the period applicable to the rebate cap
	RebateUnit      string                           // the result unit of applying rebateRate, e.g., S$, OCBC$, etc.
	RebateToDollar  func(float64) []RebateRedemption // how to convert rebate amount into actually dollar amount
	MinSpend        float64                          // minimum spend requirement, if any. 0 (default value) means no min spend required
	MinSpendPeriod  string                           // e.g., per calendar month, per 3 calendar months, per billing cycle
	StartDate       *time.Time                       // start date of the rule, if applicable
	EndDate         *time.Time                       // end date of the rule, if applicable
	FinePrints      []string                         // list of fine prints. reference [0] in text would refer to FinePrints[0], etc.
}

// RebateRedemption holds the equivalent dollar amount and associated descriptions (e.g., caveats, redeem options)
type RebateRedemption struct {
	DollarAmount float64
	Description  string
}

// AppliedRebateRule includes all fields from RebateRule, together with the raw rebate amount (dollars),
// and the actual rebate rate (can be lower if the rebate cap is reached)
type AppliedRebateRule struct {
	RebateRule
	SpendDollars        float64
	RebateAmount        float64 // taken into consideration the rebate cap
	MaxRebateDollars    float64 // taken into consideration the rebate cap. result is sorted based on this
	RebateDollars       []RebateRedemption
	MaxActualRebateRate float64 // taken into consideration the rebate cap, MaxRebateDollars/Spend
}

// Matches determines if the RebateRule can match of any of the keywords given
func (rebateRule *RebateRule) Matches(keywordSet *KeywordSet) bool {
	containsKeyword := false
	nowInDateRange := false

	if rebateRule.Keywords == nil {
		containsKeyword = true
	} else {
		for _, keyword := range rebateRule.Keywords {
			if keywordSet.Contains(keyword) {
				containsKeyword = true
				break
			}
		}
	}

	now := time.Now()
	if (rebateRule.StartDate == nil || rebateRule.StartDate.Before(now)) && (rebateRule.EndDate == nil || rebateRule.EndDate.After(now)) {
		nowInDateRange = true
	}

	return containsKeyword && nowInDateRange
}

// ApplySpend applies a dollar amount to the rebate rule to derive the actual rebate amount and rate.
func (rebateRule RebateRule) ApplySpend(spendDollars float64) AppliedRebateRule {
	rebateAmount := rebateRule.RebateRate * spendDollars
	rebateDollars := rebateRule.RebateToDollar(rebateAmount)
	// TODO sort
	maxRebateDollars := rebateDollars[0].DollarAmount
	if rebateRule.RebateCap > 0 {
		maxRebateDollars = math.Min(maxRebateDollars, rebateRule.RebateCap)
	}
	maxActualRebateRate := rebateRule.RebateRate
	if spendDollars > 0 {
		maxActualRebateRate = maxRebateDollars / spendDollars
	}

	return AppliedRebateRule{
		rebateRule,
		spendDollars,
		rebateAmount,
		maxRebateDollars,
		rebateDollars,
		maxActualRebateRate,
	}

}
